// gestion du sommeil
let sommeil = 60;
let intervalIdSom = null;

function bipsom() {
    if(sommeil == 0) finish();
    else {      
        document.getElementById("bipsom").innerHTML = sommeil + " secondes restantes";
        sommeil--;
    }   
}
function initialisesom(){
    sommeil = 60; 
}       

// gestion de la faim
let counterfaim = 40;
let intervalIdFaim = null;

function bipfaim() {
    if(counterfaim == 0) finish();
    else {      
        document.getElementById("bipfaim").innerText = counterfaim + " secondes restantes";
        counterfaim--;
    }   
}
function initialisefaim(){
    counterfaim = 40;
}       

// gestion de l'hygiène
let counterlav = 30;
let intervalIdlav = null;

function biplav() {
    if(counterlav == 0) {
        finish();
    } else {      
        document.getElementById("biplav").innerText = counterlav + " secondes restantes";
        counterlav--;
    }   
}
function initialiselav(){
    counterlav = 30;
}    

//lancement du jeu
function start(){
    initialisesom();
    initialiselav();
    initialisefaim();
    intervalIdSom = setInterval(bipsom, 1000);
    intervalIdFaim = setInterval(bipfaim, 1000);
    intervalIdlav = setInterval(biplav, 1000);
  } 

//fin du jeu
function finish() {
    clearInterval(intervalIdSom);
    document.getElementById("bipsom").innerHTML = "TERMINE!";   
    clearInterval(intervalIdFaim);
    document.getElementById("bipfaim").innerHTML = "TERMINE!";   
    clearInterval(intervalIdlav);
    document.getElementById("biplav").innerHTML = "TERMINE!";        
  }
  

